package com.example.imitproject;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

public class MainActivity extends AppCompatActivity {

    Button bt_intent,bt_send,open_activity,open_phone,open_activity_result,get_image;
    EditText my_massage;
    TextView viewResult;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bt_intent = findViewById(R.id.bt_intent);
        bt_send = findViewById(R.id.bt_send);
        my_massage = findViewById(R.id.my_massage);
        open_activity = findViewById(R.id.open_activity);
        open_phone = findViewById(R.id.open_phone);
        open_activity_result = findViewById(R.id.open_activity_result);
        get_image = findViewById(R.id.get_image);
        viewResult = findViewById(R.id.view_result);

        bt_intent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com"));
                startActivity(intent);
            }
        });
        bt_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT,"send my massage");
                intent.putExtra(Intent.EXTRA_SUBJECT,"MY SUBJECT");
                startActivity(intent);
            }
        });
        open_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentActivity = new Intent(MainActivity.this,SecActivity.class);
                intentActivity.putExtra("name",my_massage.getText().toString());
                startActivity(intentActivity);
            }
        });
        open_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                String phone = "tel:" + my_massage.getText().toString();
                intent.setData(Uri.parse(phone));
                startActivity(intent);
            }
        });


        open_activity_result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,ActivityResult.class);
                startActivityForResult(intent,100);
            }
        });
        get_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,ThirdActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == 101 && data!= null){
            String myTextFromResult = data.getStringExtra("massage");
            viewResult.setText(myTextFromResult);
        }
    }


}