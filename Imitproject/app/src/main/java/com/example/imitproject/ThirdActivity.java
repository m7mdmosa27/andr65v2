package com.example.imitproject;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class ThirdActivity extends AppCompatActivity {

    ImageView imageView;
    Button get_image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        imageView = findViewById(R.id.imageView);
        get_image = findViewById(R.id.get_image_from_gallary);

        get_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getImageFromImage();
            }
        });
    }

    private void getImageFromImage(){
//        Intent intent = new Intent(MainActivity.this, ThirdActivity.class);
        Intent getImageIntent = new Intent(Intent.ACTION_PICK);
        getImageIntent.setType("image/*");
        startActivityForResult(getImageIntent, 50);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 50 && data != null) {
//                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);

            try {
                final Uri imageUri = data.getData();
//                imageView.setImageURI(imageUri);
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                imageView.setImageBitmap(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(ThirdActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
            }

        }else {
            Toast.makeText(ThirdActivity.this, "You haven't picked Image",Toast.LENGTH_LONG).show();
        }
    }
}

// stackoverflow.com/questions/38352148/get-image-from-the-gallery-and-show-in-imageview